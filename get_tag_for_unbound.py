import json
import os

import requests


def get_tag_for_unbound():
    # url = "https://gitlab.com/api/v4/registry/repositories/3614444?tags=true"
    # headers = {"PRIVATE-TOKEN": "glpat-wq3TXBpXuvt9gTS7q5GV"}
    url = os.getenv("GITLAB_REPO_URL")
    token = os.getenv("GITLAB_TOKEN")
    response = requests.get(url, headers={"PRIVATE-TOKEN": token})
    result = json.loads(response.content)
    tags = result["tags"]
    tag_numbers = []
    for tag in tags:
        if "unbound" in tag["name"]:
            tag_number = tag["name"].split(':')
            print(tag_number)
            tag_numbers.append(float(tag_number[1]))
    return max(tag_numbers)


if __name__ == "__main__":
    max_tag_number = get_tag_for_unbound()
    unbound_tag = "unbound-{}".format(max_tag_number)
    os.environ["UNBOUND_TAG"] = unbound_tag
