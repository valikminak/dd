import json
import os
import time
import logging

from dns.resolver import NXDOMAIN, Resolver
from schedule import every, run_pending
import requests
from requests.adapters import HTTPAdapter
from urllib3.util.retry import Retry
from whois import whois


import dns.resolver

logger = logging.getLogger()
logger.setLevel(logging.INFO)
ch = logging.StreamHandler()
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)

BASIC_AUTH_USERNAME = os.getenv('BASIC_AUTH_USERNAME', 'metrics')
BASIC_AUTH_PASSWORD = os.getenv('BASIC_AUTH_PASSWORD', 'fPZhjazmNxvV2X')
MAGISTRA_GATE_URL = os.getenv('MAGISTRA_GATE_URL', 'http://127.0.0.1:8000/api-adblock-checker/')
MAGISTRA_URL = ''.join(MAGISTRA_GATE_URL.split('api-adblock-checker/'))
# SLACK_WEBHOOK_URL = 'https://hooks.slack.com/services/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'
SLACK_WEBHOOK_URL = "https://hooks.slack.com/services/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"

retry_strategy = Retry(
    total=3,
    status_forcelist=[500, 502, 503, 504],
    allowed_methods=["GET", "POST"],
    backoff_factor=1
)
adapter = HTTPAdapter(max_retries=retry_strategy)


class CheckNSDomains:
    def __init__(self, domains):
        self.domains = domains

    def check(self):
        domains_without_ns = self._get_domains_without_ns()
        if domains_without_ns:
            suspended_domains = self._check_for_suspended_domains(domains_without_ns)
            if suspended_domains:
                domains = {x: self.domains.get(x) for x in suspended_domains}
                message_text = '<WHOIS> has found suspended name servers in domains: ' + \
                               ' '.join(['<{}host_config/domain/{}/change/|{}>;'
                                        .format(MAGISTRA_URL, d_pk, d_d) for d_d, d_pk in domains.items()])
                self.__send_notice(message_text, "#test")
                logger.info("There were found domains with suspended name servers : " + ", ".join(
                    [x for x in suspended_domains]))
        else:
            logger.info(
                'There were checked {} domains. Domains with suspended name servers were not found'.format(
                    len(self.domains))
            )

    def _get_domains_without_ns(self):
        logger.info("_get_domains_without_ns")
        domains = []
        resolver = Resolver()
        resolver.nameservers = ["127.0.0.1"]
        for d in self.domains.keys():
            try:
                resolver.resolve(d, 'NS')
            except NXDOMAIN:
                domains.append(d)
            if len(domains) >= 3:
                break
        logger.info(
            f"domains without ns {domains}"
        )
        return domains

    @staticmethod
    def _check_for_suspended_domains(domains_without_ns):
        suspended_domains = []
        for domain in domains_without_ns[:3]:
            w = whois(domain)
            name_servers = list(w.name_servers)
            if "suspended" in "\t".join(name_servers).lower():
                suspended_domains.append(domain)
        logger.info(
            f"suspended domains {suspended_domains}"
        )
        return suspended_domains

    @staticmethod
    def __send_notice(text, channel='#bot'):
        """
        Send notice to slack
        """
        try:
            request = requests.Session()
            request.mount('https://', adapter)
            json_text = json.dumps({'text': text, 'channel': channel})
            r = request.post(SLACK_WEBHOOK_URL, data={'payload': json_text})
            r.raise_for_status()
        except Exception as e:
            logger.warning(e)
        else:
            return 'Success sent message to channel {}'.format(channel)


def check_name_servers():
    # try:
    #     logger.info("Checking started")
    #     response = requests.get(MAGISTRA_GATE_URL, auth=HTTPBasicAuth(BASIC_AUTH_USERNAME, BASIC_AUTH_PASSWORD))
    # except ConnectionError:
    #     logger.info("Failed to get list of domains")
    # else:
    #     if response.status_code == 200:
    #         domains = json.loads(response.content)
    domains = {"onenightwomenmey9.com": 1, "ardentgirldu.com": 2}
    adblock_checker = CheckNSDomains(domains=domains)
    adblock_checker.check()
    # finally:
    #     logger.info("Checking finished")


if __name__ == '__main__':
    logger.info("CONtainer started")
    every(10).seconds.do(check_name_servers)
    while True:
        logger.info("1")
        run_pending()
        time.sleep(5)
