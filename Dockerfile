FROM alpine:3.10

ENV PYTHONUNBUFFERED 1


RUN mkdir /app

COPY Pipfile /app/
COPY Pipfile.lock /app/
WORKDIR /app

RUN apk add --no-cache python3
RUN apk add --update bind-tools

RUN pip3 install --upgrade pip && pip3 install pipenv && pipenv install --system --deploy --ignore-pipfile


COPY main.py /app/

ENTRYPOINT python3 /app/main.py
